public class Geometry {
    int length;
    int breadth;

    Geometry(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public int calculateArea() {
        return length * breadth;
    }

    public int calculatePerimeter() {
        return (2 * (length + breadth));
    }

    public static Geometry CreateSquare(int side) {
        return new Geometry(side, side);
    }


}
