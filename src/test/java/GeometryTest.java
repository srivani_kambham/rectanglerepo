import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class GeometryTest {

    @Test
    public void shouldReturnAreaOfRectangle() {
        assertEquals(200, new Geometry(20, 10).calculateArea());
    }

    @Test
    public void shouldNotReturnAreaOfRectangle() {
        assertNotEquals(210, new Geometry(20, 10).calculateArea());
    }

    @Test
    public void ShouldReturnPerimeterOfRectangle() {

        assertEquals(60, new Geometry(20, 10).calculatePerimeter());
    }

    @Test
    public void ShouldNotReturnPerimeterOfRectangle() {

        assertNotEquals(70, new Geometry(20, 10).calculatePerimeter());
    }

    @ParameterizedTest
    @CsvSource({"10,20,200", "4,4,16", "4,3,12"})
    void shouldReturnRectangleAreaWithParameters(Integer length, Integer breadth, Integer expected) {
        int actualvalue = new Geometry(length, breadth).calculateArea();
        assertEquals(expected, actualvalue);
    }

    @ParameterizedTest
    @CsvSource({"10,20,60", "4,4,16", "4,3,14"})
    void shouldReturnRectanglePerimeterWithParameters(Integer length, Integer breadth, Integer expected) {
        int actualvalue = new Geometry(length, breadth).calculatePerimeter();
        assertEquals(expected, actualvalue);
    }

    @Test
    public void shouldReturnAreaOfCreateSquare() {

        assertEquals(400, Geometry.CreateSquare(20).calculateArea());
    }

    @Test
    public void shouldReturnPerimeterOfRectangle() {

        assertEquals(80, Geometry.CreateSquare(20).calculatePerimeter());
    }

    @ParameterizedTest
    @CsvSource({"10,100", "4,16", "3,9"})
    void shouldReturnAreaOfCreateSquareWithParameters(Integer side, Integer expected) {
        int actualvalue = Geometry.CreateSquare(side).calculateArea();
        assertEquals(expected, actualvalue);
    }

    @ParameterizedTest
    @CsvSource({"10,40", "4,16", "3,12"})
    void shouldReturnPerimeterOfCreateSquareWithParameters(Integer side, Integer expected) {
        int actualvalue = Geometry.CreateSquare(side).calculatePerimeter();
        assertEquals(expected, actualvalue);
    }

}
